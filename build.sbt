import Dependencies._

ThisBuild / scalaVersion     := "2.13.6"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "org.bitbucket.jackcviers"
ThisBuild / organizationName := "scalaFromJava"

lazy val root = (project in file("."))
  .settings(
    name := "scalafromjava",
    libraryDependencies += scalaTest % Test,
    libraryDependencies += catsEffect,
    libraryDependencies += refined,
    libraryDependencies += refinedCats,
    Compile / compileOrder := CompileOrder.ScalaThenJava
  )

