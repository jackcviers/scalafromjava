package org
package bitbucket
package jackcviers

import eu.timepit.refined.types.string
import eu.timepit.refined.api.RefinedTypeOps
import eu.timepit.refined.api.Refined
import eu.timepit.refined

package object scalaFromJava {
  type Input = string.NonEmptyString
  object Input extends RefinedTypeOps[Input, String]

  type Output = String Refined refined.string.StartsWith["O:"]
  object Output extends RefinedTypeOps[Output, String]
}
