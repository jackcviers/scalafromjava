package org
package bitbucket
package jackcviers
package scalaFromJava

import cats.effect.kernel.Sync

abstract class OutputProcessor[F[+_]] {
  def process(l: List[Input]): F[Output]
}

object OutputProcessor {
  def apply[F[+_]: Sync]: OutputProcessor[F] = implicitly
  implicit def makeInstance[F[+_]: Sync]: OutputProcessor[F] =
    new OutputProcessor[F] {
      private val effect = Sync[F]
      override def process(l: List[Input]): F[Output] = l.headOption
        .map(i =>
          Output
            .from(s"O:${i.value}")
            .fold(
              err => effect.raiseError(new RuntimeException(err)),
              succ => effect.pure(succ)
            )
        )
        .getOrElse(
          effect.raiseError[Output](new RuntimeException(s"$l was empty"))
        )
    }
}
