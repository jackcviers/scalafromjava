package org
package bitbucket
package jackcviers
package scalaFromJava

import cats._
import cats.implicits._
import cats.effect.Sync
import cats.effect.implicits._
import scala.collection.JavaConverters._
import cats.Traverse
import cats.effect.IO
import cats.effect.unsafe.implicits.global

abstract class ScalaEntry {
  def list(): java.util.List[String]
  private def listInF[F[+_]: Sync](): F[List[String]] = {
    val effect = implicitly[Sync[F]]
    effect.delay(list().asScala.toList)
  }
  private def processList[F[+_]: Sync]: F[Output] = {
    val effect = implicitly[Sync[F]]
    effect.flatMap(listInF()){l =>
      effect.flatMap(InputConverter[F].inputConverter(l)){ lAsInputs =>
        OutputProcessor[F].process(lAsInputs)
      }
    }
  }

  def processListAsIO(): Output = processList[IO].unsafeRunSync
}
