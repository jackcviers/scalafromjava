package org
package bitbucket
package jackcviers
package scalaFromJava

import cats.effect.kernel.Sync
import cats.effect.IO
import cats.syntax.traverse._
import cats.instances.list._

abstract class InputConverter[F[+_]] {
  def inputConverter(l: List[String]): F[List[Input]]
}
object InputConverter {
  def apply[F[+_]: InputConverter]: InputConverter[F] = implicitly
  implicit def makeInstance[F[+_]: Sync]: InputConverter[F] =
    new InputConverter[F] {
      private val effect = Sync[F]
      override def inputConverter(l: List[String]): F[List[Input]] = l.map {
        s =>
          Input
            .from(s)
            .fold(
              err =>
                effect.raiseError[Input](new RuntimeException(s"$s: $err")),
              succ => effect.pure(succ)
            )
      }.sequence
    }
}
