import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.2.8"
  lazy val catsEffect = "org.typelevel" %% "cats-effect" % "3.1.1"
  lazy val refined = "eu.timepit" %% "refined"                 % "0.9.26"
  lazy val refinedCats = "eu.timepit" %% "refined-cats"            % "0.9.26"
}
