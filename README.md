# README #

To compile, you need [sbt](https://www.scala-sbt.org/)

## Example calling scala from java using an abstract class extension around the scala world for compilation ###

JavaExtension.java is the entry point into the scala sources. It extends ScalaEntry.scala. Note that the scala code can have all the type shenanigans and implicits that you would like. The Java code that calls the scala method in JavaEntry doesn't have any weirdness on it:

    new JavaExtension().processListAsIO()
	
Calls the `ScalaEntry.processListAsIO`, which is written in scala, and the `JavaEntry` class extends the scala abstract class, defining the abstract member necessary for the scala code to work.

### Build and run using `sbt` ###

1. Install sbt
2. start sbt with `sbt`
3. Enter `run` at the prompt and hit `<Enter>`

As you can see, the scala code executes at runtime, using the provided list in JavaEntry, and outputs the head of the list with `O:` attached as a string.
